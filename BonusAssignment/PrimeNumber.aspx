﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrimeNumber.aspx.cs" Inherits="BonusAssignment.PrimeNumber" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
       <div>
            <h1>Divisible Sizzable</h1>
            <p>Enter a number and find out if your number is a prime number!</p>
            <br />
            <asp:Label ID="numberInputLabel" runat="server" AssociatedControlID="numberInput" Text="Please enter a integer:" Font-Bold="true"></asp:Label>
            <asp:Textbox ID="numberInput" runat="server"></asp:Textbox>
            <asp:RequiredFieldValidator ID="numberInputFieldValidator" runat="server" ControlToValidate="numberInput" ErrorMessage="Please enter an integer"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="numberInputRegex" runat="server" ControlToValidate="numberInput" ValidationExpression="[0]*[1-9][0-9]*" ErrorMessage="Number must be a positive integer."></asp:RegularExpressionValidator>
        <div> 

        <br />
        <asp:Button runat="server" ID="submitButton" OnClick="CheckPrime" Text="Submit"/>
        <br />
        <br />
        <br />
        <div id="primeNumberMessage" runat="server"></div>
    </form>
</body>
</html>
