﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StringBling.aspx.cs" Inherits="BonusAssignment.StringBling" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>String Bling</h1>
            <p>Please enter a string and we'll check and let you know if it is a palindrome!</p>
            <asp:Label ID="userStringLabel" runat="server" AssociatedControlID="userString" Text="Please enter a string:" Font-Bold="true"></asp:Label>
            <asp:Textbox ID="userString" runat="server"></asp:Textbox>
            <asp:RequiredFieldValidator ID="userStringFieldValidator" runat="server" ControlToValidate="userString" ErrorMessage="Please enter a string"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="userStringRegex" runat="server" ControlToValidate="userString" ValidationExpression="^[a-zA-Z ]+$" ErrorMessage="Input must be a string of characters."></asp:RegularExpressionValidator>
        </div>
        <br />
        <asp:Button runat="server" ID="submitButton" OnClick="CheckPalindrome" Text="Submit"/>
        <br />
        <br />
        <br />
        <div id="palindromeMessage" runat="server"></div>
    </form>
</body>
</html>
