﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CartesianSmartesian.aspx.cs" Inherits="BonusAssignment.CartesianSmartesian" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Cartesian Smartesian</h1>
            <p>Enter a co-ordinate and find out which quadrant your co-ordinate falls in!</p>
            <br />
            <asp:Label ID="xCoordinateLabel" runat="server" AssociatedControlID="xCoordinate" Text="Please enter a x co-ordinate:" Font-Bold="true"></asp:Label>
            <asp:Textbox ID="xCoordinate" runat="server" placeholder="X co-ordinate"></asp:Textbox>
            <asp:RequiredFieldValidator ID="xCoordinateFieldValidator" runat="server" ControlToValidate="xCoordinate" ErrorMessage="Please enter a x co-ordinate"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="xCoordinateRegex" runat="server" ControlToValidate="xCoordinate" ValidationExpression="[-|+]?[0]*[1-9][0-9]*" ErrorMessage="X co-ordinate must be an integer and must not equal to 0. "></asp:RegularExpressionValidator>
        <div> 
         <br />
        <div>
            <asp:Label ID="yCoordinateLabel" runat="server" AssociatedControlID="yCoordinate" Text="Please enter a y co-ordinate:" Font-Bold="true"></asp:Label>
            <asp:Textbox ID="yCoordinate" runat="server" placeholder="Y co-ordinate"></asp:Textbox>
            <asp:RequiredFieldValidator ID="yCoordinateFieldValidator" runat="server" ControlToValidate="yCoordinate" ErrorMessage="Please enter a y co-ordinate"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="yCoordinateRegex" runat="server" ControlToValidate="yCoordinate" ValidationExpression="[-|+]?[0]*[1-9][0-9]*"  ErrorMessage="Y co-ordinate must be an integer and must not equal to 0. "></asp:RegularExpressionValidator> 
        </div>
        <br />
        <asp:Button runat="server" ID="submitButton" OnClick="FindQuadrant" Text="Submit"/>
        <br />
        <br />
        <br />
        <div id="quadrantMessage" runat="server"></div>
    </form>
</body>
</html>
