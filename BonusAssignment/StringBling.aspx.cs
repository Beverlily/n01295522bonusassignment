﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class StringBling : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void CheckPalindrome(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            else
            {
                bool isPalindrome = true;
                string stringInput = userString.Text.ToLower().Replace(" ", string.Empty);

                for(int i=0; i<stringInput.Length/2; i++)
                {
                    if (stringInput[i] != stringInput[stringInput.Length - 1 - i])
                    {
                        isPalindrome = false;
                        break;
                    }
                }
                if(isPalindrome) palindromeMessage.InnerHtml = "The string, " + userString.Text + ", is a palindrome.";
                else palindromeMessage.InnerHtml = "The string,  " + userString.Text + ", is not a palindrome.";
            }
        }
    }
}