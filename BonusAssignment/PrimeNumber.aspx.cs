﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class PrimeNumber : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void CheckPrime(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            else
            {
                int userInput = int.Parse(numberInput.Text);
                bool isPrime = true;

                if (userInput==1) isPrime = false;
                //Checks to see if user number is divisible by any number that is not 1 or itself
                else
                {
                    for (int i=2; i<userInput; i++)
                    {
                        if (userInput % i == 0)
                        {
                            isPrime = false;
                            break;
                        }
                    }
                }

                if (isPrime) primeNumberMessage.InnerHtml = "Your number, " + numberInput.Text + ", is a prime number.";
                else primeNumberMessage.InnerHtml = "Your number, " + numberInput.Text + ", is not a prime number.";
            }
        }
    }
}