﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class CartesianSmartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void FindQuadrant(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            else
            {
                string message = "";
                string quadrant = "";
                int xCoord = int.Parse(xCoordinate.Text);
                int yCoord = int.Parse(yCoordinate.Text);

                //positive x coordinate
                if (xCoord > 0)
                {
                    if (yCoord > 0) quadrant = "Quadrant 1";
                    else quadrant = "Quadrant 4";
                }
                //negative x coordinate
                else
                {
                    if (yCoord > 0) quadrant = "Quadrant 2";
                    else quadrant = "Quadrant 3";
                }
                message = "The co-ordinate you've entered is: (" + xCoordinate.Text + " , " + yCoordinate.Text + "). This co-ordinate lies in " + quadrant + ".";
                quadrantMessage.InnerHtml = message;
            }
        }
    }
}